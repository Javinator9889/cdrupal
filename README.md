# CDrupal

Clean Drupal 8 project

## Installation

```bash
# Download the repository
git clone https://gitlab.com/Javinator9889/cdrupal.git

# Navigate to docker folder
cd cdrupal/docker

# Create the containers
docker-compose up --build -d

# Get the PHP container ID
docker ps | grep php  # `grep' is only available on Unix systems

# Execute a shell in the PHP container
docker exec -it CONTAINER_ID /bin/bash

####################################################
##   Goto http://drupal.docker.localhost:8000/    ##
##   and configure the site for the first time    ##
####################################################

# Annotate our own UUID
drush config-get "system.site" uuid

# Change the UUID to the new config one
drush config-set "system.site" uuid "0c933d87-0f92-4f18-b53f-2cc2b08ee83c"

# Remove previous shortcut links
drush ev '\Drupal::entityManager()->getStorage("shortcut_set")->load("default")->delete();'

# Import the new configuration
drush cim

# Rebuild caches, run cron jobs and Drupal is ready for use
drush cr && drush cron
```
